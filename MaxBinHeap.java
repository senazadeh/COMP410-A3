package MaxBinHeap_A3;

public class MaxBinHeap implements Heap_Interface {
  private double[] array; //load this array
  private int size;
  private static final int arraySize = 10000; 
  
  	//Everything in the array will initially 
    //be null. This is ok! Just build out 
    //from array[1]

  public MaxBinHeap() {
    this.array = new double[arraySize];
    array[0] = Double.NaN;  
    
    //0th will be unused for simplicity 
    //of child/parent computations...
    //the book/animation page both do this.
  }
    
  //Please do not remove or modify this method! Used to test your entire Heap.
  public double[] getHeap() { 
    return this.array;
  }
  

  	//  insert
  	//    in: a double (assume no duplicates will be inserted)
  	//    return: void
  
    // Ordering is done based on the doubles as priorities. In the test data we use, the double priorities will be unique -- 
    // there will be no duplicate values.
  
	public void insert(double element) {	
		int current = size+1; 
		int parent = current/2;
		array[current] = element;
		 
		while (true) {
			if (parent == 0 || array[current] < array[parent]) {
				size++;	
				return;
			} else {
				double temp = array[parent];
				array[parent] = array[current];
				array[current] = temp;
				current = parent;
				parent = parent/2;
			}
		} 
	}  
	 
	//	delMax
	//    in: nothing
	//    return: void
	
	// This operation removes the root element from the heap. The size of the heap goes down by 1 (if the heap was not 
	// already empty). If delMax is done on an empty heap, treat it as a no-op... i.e., do nothing other than return void.
	
	public void delMax() {
		if (size == 0) { return; }
		array[1] = array[size];
		// array[size] = Double.NaN; 
		size--;
		percolateDown(1);
	}
	
	
	public void percolateDown (int hole) {
		int child;
		Double temp = array[hole];
	
		for (; hole * 2 <= size; hole = child) {
			child = hole * 2;
			
			if (child != size && array[child+1] > array[child]) {
				child++;
			}
			if (array[child] > temp) {
				array[hole] = array[child];
			}
			else
				break;
		}
		array[hole] = temp;
	}
 
	public void helper(double[] array, int size, int index) {
		int left = index*2;
		int right = (index*2)+1;
		int max_index = index;
		
		if( left <= size && array[left] > array[max_index]) {
			max_index = left;
		}
		
		if(right <= size && array[right] > array[max_index]) {
			max_index = right;
		}
		
		if(max_index != index) {
			double temp = array[index];
			array[index] = array[max_index];
			array[max_index] = temp;
			helper(array, size, max_index);
		}
	}
	//	getMax
	//    in: nothing
	//    return: a double
	
	// This operation returns a double. It does NOT alter the heap. The heap has the same elements in the same arrangement 
	// after as it did before. If getMax is done on an empty heap, return Double.NaN .
	 
	public double getMax() {
		if (size == 0) {
			return Double.NaN;
		} else {
			return array[1];
		}
	}
	
	//	clear
	//    in: nothing
	//    return: void
	//    this sets the heap to the state it has when first created... no elements 
	//    in the heap array, size 0, next useable array slot is 1.
	
	public void clear() {
		for (int i = 0; i < arraySize; i++) {
			array[i] = 0;
		}
		array[0] = Double.NaN;
		size = 0;
	}
	
	//	size
	//    in: nothing
	//    return: integer 0 or greater
	
	// The size operation simply returns the count of how many elements are in the heap. It should be 0 if the heap 
	// is empty, and always return a 0 or greater.
	
	public int size() {
		return size;
	}
	
	//	build
	//    in: array of double that needs to be in the heap
	//    return: void
	//    (assume for a build that the bheap will start empty)
	
	// The build operation should start with an empty heap. This means empty the internal heap array first before anything 
	// else. It receives an array of elements (doubles) as input. The effect is to produce a valid heap that contains exactly
	// those input elements. This means when done the heap will have both a proper structure and will exhibit heap order.

	// Build is not the same as doing an insert for every element in the array. It is the special O(N) operation from the text 
	// (and shown in class) that starts with placing all elements into the heap array with no regard to heap order. You then 
	// go to the parent of the last node, and bubble down as needed. Go to the next node back towards the root, bubble down as 
	// needed. Repeat until you have done the root.
	
	public void build(double[] elements) {
		
		for (int i = 0; i < elements.length; i++) {
			array[i+1] = elements[i];
		}
		
		this.size = elements.length;
		
		for(int i = (int) Math.floor(this.size/2); i > 0; i-- ) {
			helper(this.array, this.size, i);
		}
	}

	//	sort
	//    in: array of double
	//    return: array of double
	
	// This method will use the head to sort... i.e., heap sort. It will receive an array of elements (double) 
	// is some order and it will return an array containing those same elements in increasing order with the smallest 
	// element in array slot 0. The sort will be done by emptying the interal heap array and then building a valid 
	// heap from the elements in the input array. Then repeated delMax() operations should be done to extract the 
	// elements in sorted order, and those elements put into the return array. Pay attention to the fact that you have 
	// max bin heap but need the sort to be increasing smallest to largest.
	
	public double[] sort(double[] elements) {
		clear();
		build(elements);
		double[] sorted = new double[elements.length];
		for (int i = sorted.length-1; i >= 0; i--) {
			sorted[i] = getMax();
			delMax();
		}
		return sorted;
	}
	
	
	
	
	
	
}
